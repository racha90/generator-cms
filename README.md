# Меню

[Старт и инициализация](#cтарт-и-инициализация)

[Удаление и очистка всех контейнеров](#удаляем-контейнеры-и-образы-docker-если-необходимо)

## Старт и инициализация

##### Создаем папку с проектом:
```bash
mkdir generator-cms.loc
```

##### Переходим в директорию проекта:

```bash
cd generator-cms.loc/
```

##### Удаляем все папки и скрытие файлы, если такие есть:

> Например PHPStorm создаёт по умолчанию папку `.idea`

```bash
rm -rf .idea/
```

##### Клонируем проект с `gitlab.com` в текущую папку:
```bash
git clone git@gitlab.com:mbkkong/generator-cms.git ./
```

##### Делаем копию .env.example файла:
```
cp .env.example .env
```
> Можем изменить или переписать данные, которые нам нужно

##### Собираем Docker, генерируем ключ, пересобираем Docker, делаем миграцию.
```
make up_dev
make key_generate
make up_dev
make migrate
```

Доступ к CLI Docker-контейнеров:
Используется для доступа к БД:
```
docker exec -it web_postgres bin/sh
```

Для работы с php artisan:
```
docker exec -it web_app /bin/sh
```

## Удаляем контейнеры и образы Docker, если необходимо
```bash
docker stop $(docker ps -a -q);
docker rm $(docker ps -a -q);
docker system prune --all --force --volumes
```
> Так же не забываем, что нам нужно еще удалить папку storage с базой данных 
