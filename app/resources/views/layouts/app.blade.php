<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>{{ $title }}</title>

    <meta name="description" content="{{ $description }}">
    <meta name="keywords" content="{{ $keywords }}">
    @include('partials.styles')
    @include('partials.scripts')

</head>

<body class="HP ru">

@if($settings['header'] === 1)
    @include('partials.header.01')
@elseif($settings['header'] === 2)
    @include('partials.header.02')
@elseif($settings['header'] === 3)
    @include('partials.header.03')
@endif

<main class="container">

    @yield('content')

</main>

<div id="branding-alert"></div>

@if($settings['footer'] === 1)
    @include('partials.footer.01')
@elseif($settings['footer'] === 2)
    @include('partials.footer.02')
@elseif($settings['footer'] === 3)
    @include('partials.footer.03')
@endif

</body>
</html>
