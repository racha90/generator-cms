@php
    $title='Lara News- Главная страница';
    $description='Lara News- Главная страница';
    $keywords='Новости, Генератор, Суперпроект';
@endphp
@extends('layouts.app')

@section('content')
    <div class="hidden-xs row media-grid">
        <div class="col-lg-12 col-md-6 col-sm-12 col">
            <div class="news-main" style="background:rgba(155,135,123,0.6);">
                <a href="{{ route('post', ['slug' => $random_posts[0]->slug]) }}">
                    <img src="https://picsum.photos/980/300"
                         alt="Весна 2019">
                </a>
                <div class="fon_white">
                    <h2>
                        <a href="{{ '/' }}">
                            {{$random_posts[0]->title}} </a>
                    </h2>
                    @foreach($random_posts[0]->categories as $category)
                        <a class="label label-main visible-lg-inline-block"
                           href="{{ route('category', ['slug' => $category->slug]) }}">
                            {{$category->name}}</a>
                    @endforeach
                    <span class="media_date visible-lg-inline-block">
                        {{$random_posts[0]->published_at}}
                    </span>
                </div>
            </div>
        </div>
    </div>

    <script>
        (function () {

            var swiperInited = false;

            function initTopCarousel() {

                new Swiper('.top-swiper', {
                    pagination: '.top-swiper .swiper-pagination',
                    paginationClickable: true
                });

                swiperInited = true;
            }

            if (Tochka.winWidth() < 600) {

                initTopCarousel();

            }
            else {

                window.addEventListener('resize', function () {

                    if (!swiperInited && Tochka.winWidth() < 600) {

                        initTopCarousel();

                    }

                });
            }

        })();
    </script>


    <div class="f-row main-row">

        <div class="f-col f-col-lg-8">


            <div class="b_header">
                <h1>Последние новости</h1>
            </div>

            <div id="feed" class="b_articles">
                @forelse($posts as $post)

                    <div class="media" data-id="{{ $post->id }}">

                        <div class="media-left">

                            <a href="{{ '/' }}"
                               style="background:rgba(133,146,146,0.6);">
                                <img
                                    data-original="https://picsum.photos/240/180"
                                    class="visible-xs-block lazy" alt="Италия достопримечательности">
                                <img
                                    data-original="https://picsum.photos/240/180"
                                    class="hidden-xs lazy" alt="Италия достопримечательности">
                            </a>

                        </div>


                        <div class="media-body">

                            <h2 class="media-heading">
                                <a href="{{ route('post', ['slug' => $post->slug]) }}">{{ $post->h1 }}</a>
                            </h2>

                            @foreach($post->categories as $category)
                                <a class="label label-main hidden-xs"
                                   href="{{ route('category', ['slug' => $category->slug]) }}">{{ $category->name }}</a>
                            @endforeach

                            <span class="media_date hidden-xs">{{ $post->published_at }}</span>

                            <p class="hidden-xs">
                                <a href="{{ route('post', ['slug' => $post->slug]) }}">{{ $post->description }}</a>
                            </p>

                        </div>
                    </div>
                @empty
                    <h1>Тут ничего нет</h1>

                @endforelse
                {{ $posts->links() }}
            </div>


        </div>

        <div class="f-col f-col-lg-4 visible-lg">

            <script>

                (function () {

                    var conferences_carousel = document.getElementById('conferences_carousel');//$('#conferences_carousel');

                    function checkConferenceButtons() {

                        if ($('.item:first', conferences_carousel).hasClass('active')) {

                            $('.left.carousel-control', conferences_carousel).hide();

                        }
                        else {

                            $('.left.carousel-control', conferences_carousel).show();

                        }

                        if ($('.item:last', conferences_carousel).hasClass('active')) {

                            $('.right.carousel-control', conferences_carousel).hide();

                        }
                        else {

                            $('.right.carousel-control', conferences_carousel).show();

                        }
                    }


                    checkConferenceButtons();


                    $(conferences_carousel).on('slid.bs.carousel', function () {

                        checkConferenceButtons();

                        $("img.lazy", conferences_carousel).lazyload();

                    });

                    $("img.lazy", conferences_carousel).lazyload({
                        skip_invisible: true
                    });

                })();

            </script>

            <div id="sb5c78252cc4df7" class="stickyblock">

                <!-- BANNER v2 key="www/main" type="right3" --><!-- BANNER premium_3 OFF C1 -->
                <!-- BANNER premium_3_1 OFF C1 --><!-- BANNER premium_3_2 OFF C1 -->
                <div class="b_header">
                    <h3>информация</h3>
                </div>

                <div class="info_block">


                    <ul class="info_links list-unstyled">
                        <li><a href="#" onclick="writeToUs(); return false;">Напишите нам</a></li>
                        <li><a href="{{ '/' }}">Контакты</a></li>
                        <li><a href="{{ '/' }}">Карта сайта</a></li>
                        <li><a href="{{ '/' }}">Наши спецпроекты</a></li>
                    </ul>

                </div>

            </div>

            <script>
                $(function () {

                    if (Tochka.winWidth() < 1020) {
                        return;
                    }

                    $('#sb5c78252cc4df7').stick_in_parent({
                        parent: 'main .main-row, .Main .main-row, .Main',
                        offset_top: 60,
                        recalc_every: 20
                    });

                });
            </script>
        </div>

    </div>
@endsection
