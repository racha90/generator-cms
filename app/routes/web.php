<?php

Auth::routes();

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/home', 'HomeController@index')->name('admin.home');
    Route::get('/posts', 'PostsController@index')->name('admin.posts');
    Route::get('/categories', 'CategoriesController@index')->name('admin.categories');

    Route::get('/settings', 'SettingController@index')->name('admin.settings');
    Route::put('/settings/update', 'SettingController@update')->name('admin.settings.update');

    Route::get('/categories/edit/{id}', 'CategoriesController@edit')->name('admin.categories.edit');
    Route::put('/categories/update/{id}', 'CategoriesController@update')->name('admin.categories.update');
    Route::delete('/categories/delete/{id}', 'CategoriesController@destroy')->name('admin.categories.delete');
    Route::get('/categories/create', 'CategoriesController@create')->name('admin.categories.create');
    Route::post('/categories/store', 'CategoriesController@store')->name('admin.categories.store');

    Route::get('/posts/edit/{id}', 'PostsController@edit')->name('admin.posts.edit');
    Route::put('/posts/update/{id}', 'PostsController@update')->name('admin.posts.update');
    Route::delete('/posts/delete/{id}', 'PostsController@destroy')->name('admin.posts.delete');
    Route::get('/posts/create', 'PostsController@create')->name('admin.posts.create');
    Route::post('/posts/store', 'PostsController@store')->name('admin.posts.store');
});


Route::get('/', 'HomeController@index')->name('home');

Route::get('/categories/{slug}', 'CategoriesController@show')->name('category');
Route::get('/posts/{slug}', 'PostsController@show')->name('post');

Route::post('/posts/{slug}','CommentsController@store')->name('comment');

//Route::get('upload',['as' => 'upload_form', 'uses' => 'UploadController@getForm']);
//Route::post('upload',['as' => 'upload_file','uses' => 'UploadController@upload']);




