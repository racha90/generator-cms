<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CategoryPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $howMachCategory = 25;
        $howMachPosts = 4000;
        foreach (range(1, $howMachPosts) as $index) {
            DB::table('category_post')->insert([
                'category_id' => $faker->numberBetween($min = 1, $max = $howMachCategory),
                'post_id' => $faker->numberBetween($min = 1, $max = $howMachPosts/2),
            ]);
        }
    }
}
