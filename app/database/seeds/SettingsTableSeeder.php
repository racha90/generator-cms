<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $array = [
                'header',
                'navbar',
                'promobar',
                'footer',
                'category',
                'post',

                ];
        foreach (range(1, count($array)) as $index) {
            DB::table('settings')->insert([
                'part' => $faker->unique()->randomElement($array),
                'value' => 1,
                'max_value' => 3,
            ]);
        }
    }
}
