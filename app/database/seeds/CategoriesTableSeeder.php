<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        $howMach = 25;
        foreach (range(1,$howMach) as $index) {
            DB::table('categories')->insert([
                'name' =>       $faker->unique()->word,
                'title' =>      $faker->unique()->words($nb=3, $asText=true),
                'slug' =>       $faker->unique()->slug,
                'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
                'h1' =>         $faker->words($nb=4, $asText=true),
                'parent_id' =>  ($index < 6) ? 0 : $faker->numberBetween($min = 1, $max = 5),
            ]);
        }
    }
}
