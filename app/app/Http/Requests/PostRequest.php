<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'h1'            => 'required|max:255',
            'title'         => 'required|max:255',
            'slug'          => 'sometimes|max:255',
            'description'   => 'required|max:255',
            'text'          => 'required',
            'published_at'  => 'required|date',
            'parent_id'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'h1.required'           => 'H1 обязательна для заполнение',
            'title.required'        => 'Заголовок обязательна для заполнение',
            'slug.sometimes'        => 'Slug обязательна для заполнение',
            'description.required'  => 'Описание обязательна для заполнение',
            'text.required'         => 'Текст обязателен для заполнение',
            'published_at.required' => 'Дата публикации обязательна',
            'published_at.date'     => 'Дата должна быть в формате YYYY-MM-DD HH:MM:SS',
            'parent_id.required'    => 'Главная категория обязательно',
            'parent_id.integer'     => 'Parent_Id должно быть числом',
        ];
    }
}
