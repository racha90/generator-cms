<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Setting;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function settings()
    {
        // Берем данные из базы настроек и делаем массим ключ значение для
        // Удобной работв в blade
        // И возварашаем обратно
        $settingsBase = Setting::all();
        $settings     = [];
        foreach ($settingsBase as $setting) {
            $settings[$setting->part] = $setting->value;
        }
        return $settings;
    }
}
