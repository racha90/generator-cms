<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [
    'name', 'text', 'post_id'];

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
